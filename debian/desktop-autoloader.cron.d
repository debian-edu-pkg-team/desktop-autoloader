# After a system has booted, check what time it is and pre-load the
# filesystem cache with a standardized desktop session

SHELL=/bin/bash

@reboot         desktop-autoloader [ $(date +\%s) -lt $(date -d 07:30 +\%s) ] && sleep "$(( ( RANDOM \% 200 )  + 61 ))" && desktop-autoloader
